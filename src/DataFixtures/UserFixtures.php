<?php

namespace App\DataFixtures;

use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Faker;

class UserFixtures extends Fixture
{
    /**
     * @var UserPasswordEncoderInterface
     */
    private $passwordEncoder;

    /**
     * UserFixtures constructor.
     * @param UserPasswordEncoderInterface $passwordEncoder
     */
    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
    {
        $this->passwordEncoder = $passwordEncoder;
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $faker = Faker\Factory::create('fr_FR');
        $arabicFacker = Faker\Factory::create('ar_SA');
        $role = ['ROLE_USER','ROLE_ADMIN'];

        for($i=0;$i<10;$i++){

            $user = new User();

            $user->setEmail($faker->email);
            $user->setFonction('ناشط حقوقي سياسي');
            $user->setFullName($arabicFacker->name);
            $user->setIsActive(true);
            $user->setPicture($faker->imageUrl(null,null,"people"));
            $user->setRoles([$role[rand(0,1)]]);
            $password = "password".$i;
            $user->setPassword($this->passwordEncoder->encodePassword(
                $user,
                $password
            ));
            $manager->persist($user);
        }


        $manager->flush();
    }
}
