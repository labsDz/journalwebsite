<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Faker;

class ArticleFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {

        $faker = Faker\Factory::create('ar_SA');
        $titre = 'الحكومة السريلانكية تصر على إحراق جثث موتى المسلمين بسبب كورونا';
        $category = new Category();
        $category->setName('مستجدات');
        $manager->persist($category);

        for ($i = 0; $i < 10; $i++) {
            $article = new Article();
            $article->setAuthor($faker->name);
            $article->setIsActive(true);
            $article->setContent($faker->realText());
            $article->setTitle($titre);
            $article->setCategory($category);
            $article->setPhotoPath($faker->imageUrl());
            $article->setType('main');

            $manager->persist($article);
        }

        $manager->flush();
    }
}
