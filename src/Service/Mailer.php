<?php


namespace App\Service;


use Symfony\Component\Mailer\MailerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;


class Mailer
{
    /**
     * @var MailerInterface
     */
    private $mailer;

    /**
     * à configurer
     */
    const FROM = 'contact@haddra.com';

    /**
     * Mailer constructor.
     * @param MailerInterface $mailer
     */
    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param $to
     * @param $template
     * @param array $params
     * @param string $subject
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function accountConfirmation($to,$template,$params = [], $subject = "تأكيد الحساب على موقع haddra.com")
    {
        $email = (new TemplatedEmail())
            ->from(self::FROM)
            ->to($to)
            ->subject($subject)
            ->htmlTemplate($template)
            ->context($params)
        ;

        $this->mailer->send($email);
    }


}