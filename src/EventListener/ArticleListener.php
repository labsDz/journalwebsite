<?php

namespace App\EventListener;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

class ArticleListener
{
    /**
     * @var Security
     */
    private $security;

    /**
     * ArticleListener constructor.
     * @param Security $security
     */
    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * @param LifecycleEventArgs $args
     * @return null|object
     */
    public function prePersist(LifecycleEventArgs $args)
    {
        $article = $args->getEntity();

        if ( !$article instanceof Article ) {
            return null;
        }

        if (null === $article->getUser()) {
            $user = $this->security->getUser();

            if ($user instanceof User) {
                $article->setUser($user);
            }
        }

        return $article;
    }
}
