<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

   /**
     * @return Article[] Returns an array of Article objects
   */

    public function findByTag($value)
    {
        return $this->createQueryBuilder('a')
            ->leftJoin('a.tags', 't')
            ->andWhere('t.keyWord = :val')
            ->andWhere('a.isValidated = true')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->getQuery()
            ->getResult()

        ;
    }



    public function findByCategory( int $id)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.Category = :val')
            ->andWhere('a.isActive = true')
            ->setParameter('val', $id)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
     * @param \DateTime $validatedAt
     * @return mixed
     * @throws \Exception
     */
    public function findBlogNotificationsByUser(User $user)
    {
        return $this->createQueryBuilder('a')
            ->Where('a.user = :id')
            ->setParameter('id',$user->getId())
            ->andWhere('a.type = :type')
            ->setParameter('type','blog')
            ->getQuery()
            ->getResult();
    }

}
