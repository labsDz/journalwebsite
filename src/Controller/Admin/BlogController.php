<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Article;
use App\Form\ArticleType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BlogController
 * @package App\Controller\Admin
 * @Route("/admin/blog")
 */
class BlogController extends BaseController
{

    /**
     * @Route("/", name="admin_blog_home")
     */
    public function index()
    {

        return $this->render('admin/blog/index.html.twig', [
            'countBlogs' => count($this->getNewBlogs(false)),
            'countBlogsAll' => count($this->getNewBlogs(true)),
            'countUsers' => count($this->getUsers(50)),
            'countBlogsClosed' => count($this->getNewBlogs(true , false)),
        ]);
    }

    /**
     * @Route("/list", name="admin_blog_list_new")
     */
    public function listNewBlog()
    {
        return $this->render('admin/blog/list.html.twig', [
            'blogs' => $this->getNewBlogs(false , true )
        ]);
    }

    /**
     * @Route("/all", name="admin_blog_list_all")
     */
    public function listAllBlog()
    {
        return $this->render('admin/blog/list.html.twig', [
            'blogs' => $this->getNewBlogs(true , true )
        ]);
    }

    /**
     * @Route("/closed", name="admin_blog_list_closed")
     */
    public function listClosedBlog()
    {
        return $this->render('admin/blog/list.html.twig', [
            'blogs' => $this->getNewBlogs(true , false )
        ]);
    }


    /**
     * @Route("/validate/{slug}", name="admin_blog_validate")
     */
    public function validateBlog(string $slug , Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository(Article::class)->findOneBy(['slug' => $slug]);

        $form = $this->createForm(ArticleType::class, $blog);
        $form->handleRequest($request);
        
        if ($form->isSubmitted()) {
            $blog->setPhotoPath($form->getData()->getPhotoPath());
            $blog->setIsValidated(true);
            $blog->setPublishedAt(new \DateTime());
            $em->persist($blog);
            $em->flush();


            $this->addFlash('succes', 'المقالة مفعلة بنجاح . ');

            return $this->redirectToRoute('admin_blog_list_new');
        }

        return $this->render('admin/user_profil/add_article.html.twig', [
            'myForm' => $form->createView(),
        ]);


    }

    /**
     * @Route("/users", name="admin_blog_users")
     * @return \Symfony\Component\HttpFoundation\Response
     *
     */
    public function ActiveUsers()
    {
        $users = $this->getUsers(50);
        return $this->render('admin/blog/users.html.twig',[
            'users' => $users
        ]);
    }

    /**
     * @Route("/close/{id}", name="admin_blog_close")
     * @param int $id
     * @return \Symfony\Component\HttpFoundation\RedirectResponse
     */
    public function closeBlog(int $id)
    {
        $em = $this->getDoctrine()->getManager();
        $blog = $em->getRepository(Article::class)->findOneBy(['id' => $id]);
        $blog->setIsActive(false);

        $em->persist($blog);
        $em->flush();

        $this->addFlash('danger', 'تم إيقاف تفعيل المقالة بنجاح . ');

        return $this->redirectToRoute('admin_blog_list_all');
    }



    /**
     * @param bool $valid
     * @return object[]
     */
    private function  getNewBlogs(bool $valid = false , bool $active = true )
    {
        return $this->getDoctrine()->getRepository(Article::class)->findBy(
            ['type' => 'blog' , 'isValidated' => $valid , 'isActive' => $active ],['createdAt' => 'DESC']
        );
    }


}
