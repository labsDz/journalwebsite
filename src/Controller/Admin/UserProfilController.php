<?php

namespace App\Controller\Admin;

use App\Controller\BaseController;
use App\Entity\Article;
use App\Entity\Image;
use App\Entity\Tag;
use App\Entity\Tweet;
use App\Entity\User;
use App\Form\ArticleType;
use App\Form\ImageType;
use App\Form\TweetType;
use App\Form\UserType;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\String\Slugger\SluggerInterface;

/**
 * Class UserProfilController
 * @package App\Controller\Admin
 * @IsGranted("ROLE_USER")
 */
class UserProfilController extends BaseController
{
    /**
     * @Route("/user/profil", name="admin_user_profil")
     * @IsGranted("ROLE_USER")
     */
    public function index(ArticleRepository $repository)
    {
        $this->denyAccessUnlessGranted('IS_AUTHENTICATED_FULLY');
        $user = $this->getUser();
        $blogs = $repository->findBlogNotificationsByUser($user);
        $now = new \DateTime();
        $data = [];
        foreach ($blogs as $blog){
            if($blog->getPublishedAt() !== null and $now < $blog->getPublishedAt()->add(new \DateInterval('P1D')) ){
                array_push($data, $blog);
            }
        }
        return $this->render('admin/user_profil/user.html.twig', [
            'user' => $user,
            'stats' => $this->getUserStats($user),
            'notifications' => $data
        ]);
    }

    /**
     * @Route("/user/article/add/{slug}", defaults={"slug"=null}, name="admin_user_add_article")
     */
    public function addArticle(Request $request, ? string $slug)
    {
        $entityManager = $this->getDoctrine()->getManager();
        if(null === $slug){
            $article = new Article();
            $tag = new  Tag();
            $article->addTag($tag);
        }else{
            $article = $entityManager->getRepository(Article::class)->findOneBy(['slug' => $slug]);

        }

        $form = $this->createForm(ArticleType::class, $article);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {

            $article->setType('blog');
            $article->setIsValidated(false);
            $article->setIsActive(true);
            $article->setAuthor($this->getUser()->getFullName());

            $entityManager->persist($article);
            $entityManager->flush();

            $this->addFlash('success', 'تم إرسال المقال بنجاح .. سيصلك إشعار فور نشره .  ');
            return $this->redirectToRoute('admin_user_profil');
        }

        return $this->render('admin/user_profil/add_article.html.twig', [
            'myForm' => $form->createView(),
        ]);


    }

    /**
     * @Route("/user/profil/edit/{id}", name="admin_user_profil_edit")
     * @IsGranted("ROLE_USER")
     */
    public function editUser(int $id, Request $request, SluggerInterface $slugger)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id]);

        if (false === $this->isAllowedToEdit($user)){
            throw new \LogicException('لا يمكنك الوصول لهذا الحساب .');
        }

        $oldFileName = $user->getPicture();
        if(null !== $oldFileName){
            $oldFile = $this->getParameter('pictures_directory').'/'.$oldFileName;
        }else{
            $oldFile = null;
        }


        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $picture = $form->get('picture')->getData();

            if (null !== $picture || $picture != $user->getPicture()) {

                $originalFilename = pathinfo($picture->getClientOriginalName(), PATHINFO_FILENAME);
                // this is needed to safely include the  name as part of the URL
                $safeFilename = $slugger->slug($originalFilename);
                $newFilename = $safeFilename . '-' . uniqid() . '.' . $picture->guessExtension();
                try {
                    $picture->move(
                        $this->getParameter('pictures_directory'),
                        $newFilename
                    );
                } catch (FileException $e) {

                }

                if (file_exists($oldFile)){
                    unlink($oldFile);  // to remove the old file
                }
                $user->setPicture($newFilename);
            }
            if($user->getPicture() == $picture){

                $user->setPicture($oldFileName);
            }

            $user->SetUpdatedAt(new \DateTime());

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($user);
            $entityManager->flush();

            $this->addFlash('success', 'تم تعديل ملفك بنجاح . ');
            return $this->redirectToRoute('admin_user_profil');
        }
        return $this->render('admin/user_profil/edit_user.html.twig', [
            'myForm' => $form->createView(),
            'stats' => $this->getUserStats($user)
        ]);
    }

    /**
     * @Route("/tweet/add", name="add_tweet")
     * @IsGranted("ROLE_USER")
     */
    public function addTweet(Request $request)
    {
        $entityManager = $this->getDoctrine()->getManager();
        $tweet = new Tweet();
        $form = $this->createForm(TweetType::class, $tweet);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $tweet->setIsValide(false);
            $tweet->setUser($this->getUser());
            $entityManager->persist($tweet);
            $entityManager->flush();
            $this->addFlash('success','لقد تم إرسال التغريدة بنجاح , ستتلقى إشعار فور نشرها ');
            return $this->redirectToRoute('admin_user_profil');
        }

        return $this->render('admin/user_profil/add_tweet.html.twig', [
            'myForm' => $form->createView()
        ]);

        }

    /**
     * @param User $user
     * @return bool
     */
    private function isAllowedToEdit(User $user)
    {
        if($user === $this->getUser()){
            return true;
        }
        return false;
    }




}
