<?php


namespace App\Controller;


use App\Entity\Article;
use App\Entity\Tag;
use App\Entity\Tweet;
use App\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class BaseController extends AbstractController
{

    /**
     * @param int $limit
     * @return array
     */
    protected function getUsers(int $limit = 7)
    {
        $data = [];
        $userRepository = $this->getDoctrine()->getRepository(User::class);
        $users = $userRepository->findBy(['isActive' => true],['createdAt' => 'DESC'], $limit);

        foreach ($users as $user){
            if (true === $user->hasRole('ROLE_USER')){
                array_push($data, $user);
            }
        }
        return $data;
    }

    /**
     * @param User $user
     * @return array
     */
    protected function getUserStats(User $user)
    {
        $data = [];
        $articles = $user->getArticles();
        $data["totalArticles"]  = count($articles);
        $countViews = 0;
        $countComments = 0;

        foreach ($articles as $article){
            $countViews += $article->getClicks();
            $countComments += $article->getCommentsCount();
        }
        $data["totalViews"] = $countViews;
        $data["totalComment"] = $countComments;

        return $data;
    }

    /**
     * @return array
     */
    protected function getMoreInteractiveArticles():array
    {
        $articleRepository = $this->getDoctrine()->getRepository(Article::class);
        return  $articleRepository->findBy([],['commentsCount' => 'DESC','clicks' => 'DESC'],5);
    }

    /**
     * @return array
     */
    protected function getMoreUsedTags()
    {
        $tags = $this->getDoctrine()->getRepository(Tag::class)->findAll();
        $data = [];
        foreach ($tags as $tag) {
            array_push($data , $tag->getkeyWord());
        }
        $result = array_unique($data);
        return $result;
    }

    /**
     * @param String $slug
     * @return array
     */
    protected function getTagsByArticle( String $slug)
    {
        $tags = $this->getDoctrine()->getRepository(Tag::class)->findTagByArticle($slug);
        return $tags;

    }

    /**
     * @return object[]
     */
    protected function getTweets(int $nbr)
    {
        $tweets = $this->getDoctrine()->getRepository(Tweet::class)->findBy(
            ['isValide' => true],
            ['createdAt' => 'DESC'],
            $nbr
        );
        return $tweets;
    }

}