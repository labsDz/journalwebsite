<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Comment;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ArticleController extends BaseController
{

    /**
     * @Route("/article/{slug}", name="article_show")
     */
    public function show(string $slug, EntityManagerInterface $entityManager, Request $request)
    {
        $articleRepository = $entityManager->getRepository(Article::class);
        $article = $articleRepository->findOneBy(['slug' => $slug]);

        if($article->getType() == 'blog' && $article->getIsValidated() == false){
            return $this->redirect($request->headers->get('referer'));
        }

        $trending = $articleRepository->findBy([],['commentsCount' => 'DESC'],5);
        $clicks = $article->getClicks();
        $clicks++;
        $article->setClicks($clicks);

        $entityManager->persist($article);
        $entityManager->flush();

        return $this->render('article/detail.html.twig', [
            'article'  => $article,
            'trending' => $trending,
            'tags'     => $this->getTagsByArticle($slug)
        ]);
    }

    /**
     * @Route("/comment/add", name="comment_add")
     */
    public function addComment(Request $request, EntityManagerInterface $entityManager)
    {
        $comment = new Comment();
        $slug = $request->get('article');
        $article = $this->getDoctrine()->getRepository(Article::class)->findOneBy(["slug" => $slug]);

        $comments = count($article->getComments());
        $comments ++;
        $article->setCommentsCount($comments);
          if(!empty($request->get('content')) && !empty($request->get('author')) && !empty($request->get('email')) ){
              $comment->setContent($request->get('content'));
              $comment->setAuthor($request->get('author'));
              $comment->setEmail($request->get('email'));
              $comment->setArticle($article);

              $entityManager->persist($comment);
              $entityManager->persist($article);
          }else{
              $this->addFlash('error','الرجاء إدخال كامل المعلومات .');
              $this->redirectToRoute("article_show" , ["slug" => $slug]);
          }


        $entityManager->flush();

        return $this->redirectToRoute("article_show", ["slug" => $slug]);
    }

    /**
     * @Route("/tweets", name="tweets_show")
     */
    public function showTweets()
    {
        return $this->render('app/all_tweets.html.twig', [
            'tweets'     => $this->getTweets(8),
            'all_tweets' => $this->getTweets(100)
        ]);
    }

}
