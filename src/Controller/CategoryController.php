<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Category;
use App\Repository\ArticleRepository;
use App\Repository\TagRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CategoryController extends BaseController
{
    /**
     * @Route("category/{id}", name="category-show")
     */
    public function show(int $id)
    {

        $category = $this->getDoctrine()->getRepository(Category::class)->findOneBy(['id' => $id]);
        $articles = $category->getArticles();
        $trending = $this->getMoreInteractiveArticles();
        $contributions = $this->getContributedArticles();

        return $this->render('category/index.html.twig', [
            'articles' => $articles,
            'category' => $category,
            'trending' => $trending,
            'tags' => $this->getMoreUsedTags(),
            'contributions' =>$contributions
         ]);
    }

    /**
     * @Route("tag/{keyWord}", name="tag_list")
     */
    public function tags(String $keyWord , ArticleRepository $articleRepository )
    {
        $this->getMoreUsedTags();
        $contributions = $this->getContributedArticles();

       $articles = $articleRepository->findByTag($keyWord);

        return $this->render('category/index.html.twig', [
            'articles' => $articles,
            'category' => null,
            'trending' => $this->getMoreInteractiveArticles(),
            'tags' => $this->getMoreUsedTags(),
            'contributions' =>$contributions
        ]);

    }
    /**
     * @return Article[]
     */
    private function getContributedArticles()

    {
        $articleRepository = $this->getDoctrine()->getRepository(Article::class);
        return $articleRepository->findBy(['type' => 'contribution'], ['createdAt' => 'DESC'], 6);
    }





}
