<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Contact;
use App\Entity\News;
use App\Entity\User;
use App\Form\ContactType;
use App\Repository\ArticleRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


/**
 * Class AppController
 * @package App\Controller
 */
class AppController extends BaseController
{
    /**
     * @var ArticleRepository
     */
    private $articleRepository;

    /**
     * AppController constructor.
     * @param ArticleRepository $articleRepository
     */
    public function __construct(ArticleRepository $articleRepository)
    {
        $this->articleRepository = $articleRepository;
    }

    /**
     * return home page
     * @Route("/", name="app")
     */
    public function index()
    {
        $mainArticles = $this->getMainArticles();
        $lastNews = $this->getLastNews();
        $contributions = $this->getContributedArticles();
        $trending = $this->getMoreInteractiveArticles();
        $blogs = $this->getBlogsByUser();
        $users = $this->getUsers();

        return $this->render('app/index.html.twig', [
            'main' => $mainArticles,
            'lastNews' => $lastNews,
            'contributions' => $contributions,
            'users' => $users,
            'trending' => $trending,
            'blogs' => $blogs,
            'tags' => $this->getMoreUsedTags(),
            'tweets' => $this->getTweets(4),

        ]);
    }

    /**
     * @Route("users/list", name="users_list")
     *
     */
    public function showUsers()
    {
        $users = $this->getUsers(50);

        return $this->render('app/all_users.html.twig',[
            'users' => $users
        ]);

    }

    /**
     * @Route("users/show/{id}", name="users_show_public")
     */
    public function showUserPublic(int $id)
    {
        $user = $this->getDoctrine()->getRepository(User::class)->findOneBy(['id' => $id, 'isActive' => true ]);
        if (null === $user) {
            throw new \LogicException("لا يوجد هذا المستعمل");
        }

        return $this->render('admin/user_profil/user.html.twig', [
            'user' => $user,
            'stats' => $this->getUserStats($user),
            'activeArticles' => $this->getActiveArticles($user),
            'notifications' => []

        ]);

    }

    /**
     * return about page
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('app/about.html.twig',[
            'trending'=> $this->getMoreInteractiveArticles()
        ]);
    }

    /**
     * return contact page
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request)
    {
        $contact = new Contact();
        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm(ContactType::class, $contact);
        $form->handleRequest($request);
        if($form->isSubmitted() && $form->isValid()){
            $now = new \DateTime();
            $contact->setCreatedAt($now);
            $em->persist($contact);
            $em->flush();

        }
        return $this->render('app/contact.html.twig',[
            'trending'=> $this->getMoreInteractiveArticles(),
            'contactForm'=>$form->createView()
        ]);
    }

    /**
     * return about page
     * @Route("/privacy", name="privacy")
     */
    public function privacy()
    {

        return $this->render('app/privacy.html.twig',[
            'trending'=> $this->getMoreInteractiveArticles()
        ]);
    }

    /**
     * @param User $user
     * @return array
     */
    private function getActiveArticles(User $user)
    {
        $articles = $user->getArticles();
        $data = [];
        foreach ($articles as $article){
            if($article->getIsValidated()){
                array_push($data,$article);
            }
        }
        return $data;

    }

    /**
     * @return mixed
     */
    private function getMainArticles()
    {
        return $this->articleRepository->findBy(['type' => 'main', 'isActive' => true],['createdAt' => 'DESC'],1);
    }

    /**
     * @return Article[]
     */
    private function getLastNews()
    {
        $newsRepository = $this->getDoctrine()->getRepository(News::class);
        return $newsRepository->findBy([ 'isActive' => true]);
    }

    /**
     * @return Article[]
     */
    private function getContributedArticles()

    {
        return $this->articleRepository->findBy(['type' => 'contribution'], ['createdAt' => 'DESC'], 6);
    }

    /**
     * @return Article[]
     */
    private function getBlogsByUser()
    {
        return $this->articleRepository->findBy(
            ['type' => 'blog', 'isActive' => true ,'isValidated' => true],
            ['createdAt' => 'DESC'],15
        );
    }


}
