<?php

namespace App\Controller;

use App\Entity\User;
use App\Service\Mailer;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;

/**
 * Class SecurityController
 * @package App\Controller
 */
class SecurityController extends AbstractController
{
    /**
     * @Route("/login", name="app_login")
     */
    public function login(AuthenticationUtils $authenticationUtils): Response
    {
        // if ($this->getUser()) {
        //     return $this->redirectToRoute('target_path');
        // }

        // get the login error if there is one
        $error = $authenticationUtils->getLastAuthenticationError();
        // last username entered by the user
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/signin.html.twig', [
            'last_username' => $lastUsername,
            'error' => $error
        ]);
    }

    /**
     * @Route("/logout", name="app_logout")
     */
    public function logout()
    {
        throw new \LogicException('This method can be blank - it will be intercepted by the logout key on your firewall.');
    }

    /**
     * @Route("/reset", name="reset_psw")
     *
     * @param Request $request
     * @param TokenGeneratorInterface $generator
     * @param Mailer $mailer
     * @return Response
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function resetPsw(Request $request, TokenGeneratorInterface $generator, Mailer $mailer)
    {
        $em = $this->getDoctrine()->getManager();
        $email = $request->request->get('email');

        if ($email){
            $token = $generator->generateToken();
            $user = $em->getRepository(User::class)->findOneBy(['email' => $email]);

            if (!$user){
                $this->addFlash('error', 'يوجد خطأ , يرجى إعادة المحاولة');
                return $this->redirectToRoute('reset_psw');
            }
            $user->setToken($token);

            $mailer->accountConfirmation(
                $email, 'emails/reset.html.twig', ['user' => $user], 'إسترجاع كلمة المرور');
            $em->persist($user);
            $em->flush();
            $this->addFlash('success', 'يرجى مراجعة بريدكم الإلكتروني , لقد تم إرسال رابط لاسترجاع كلمة المرور ');

            return $this->redirectToRoute('app_login');
        }



        return $this->render('security/reset.html.twig');
    }

    /**
     * @Route("/request/psw/{token}", name="new_psw")
     */
    public function newPsw (Request $request,UserPasswordEncoderInterface $passwordEncoder, $token)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $em->getRepository(User::class)->findOneBy(['token' => $token]);
        if (!$user){
            $this->addFlash('error', 'يوجد خطأ , يرجى إعادة المحاولة');
            return $this->redirectToRoute('app_login');
        }

        if ($request->request->get('password')){
            if ($request->request->get('password') == $request->request->get('confirmPassword')){
                $user->setPassword(
                    $passwordEncoder->encodePassword(
                        $user,
                        $request->request->get('password')
                    ));
                $user->setToken(null);
                $user->setUpdatedAt(new \DateTime());

                $em->persist($user);
                $em->flush();

                $this->addFlash('success', 'تم تغيير كلمة المرور بنجاح ');
                return $this->redirectToRoute('app_login');
            }else{
                $this->addFlash('error', 'يرجى التأكد من كتابة كلمة المرور بشكل صحيح ');
                return $this->redirectToRoute('new_psw',['token' => $token]);

            }

        }

        return $this->render('security/reset-request.html.twig');

    }
}
